package ${cloudSysEngName};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
<#if cloudRegisteCenter=="eureka">
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
</#if>
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
<#if cloudRegisteCenter=="eureka">
@EnableEurekaClient
</#if>
@EnableDiscoveryClient
public class SystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemApplication.class, args);
	}

}
