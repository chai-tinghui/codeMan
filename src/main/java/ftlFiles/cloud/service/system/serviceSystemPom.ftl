<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>sys-parent</artifactId>
        <groupId>cloud</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>service-system</artifactId>

    <dependencies>
      <#if cloudRegisteCenter=="eureka">
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
      </#if>
      <#if cloudRegisteCenter=="nacos">
          <dependency>
              <groupId>com.alibaba.cloud</groupId>
              <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
          </dependency>
      </#if>
        <!--引入公共组件-->
        <dependency>
            <groupId>cloud</groupId>
            <artifactId>sys-common</artifactId>
            <version>1.0-SNAPSHOT</version>
       <#if isCloudModel && loginModel == "静态用户" && !isAuthority>
            <exclusions>
                <exclusion>
                    <groupId>mysql</groupId>
                    <artifactId>mysql-connector-java</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.postgresql</groupId>
                    <artifactId>postgresql</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.ojdbc</groupId>
                    <artifactId>ojdbc</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.microsoft.sqlserver</groupId>
                    <artifactId>sqljdbc6.0</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.microsoft.sqlserver</groupId>
                    <artifactId>sqljdbc6.0</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.mybatis.spring.boot</groupId>
                    <artifactId>mybatis-spring-boot-starter</artifactId>
                </exclusion>
            <#if databasePool == "Druid">
                <exclusion>
                    <groupId>com.alibaba</groupId>
                    <artifactId>druid-spring-boot-starter</artifactId>
                </exclusion>
            </#if>
            </exclusions>
       </#if>
        </dependency>
    </dependencies>
    <build>
        <finalName>service-system</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
