package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant;

/**
 * @ClassName MoveType
 * @Author zrx
 * @Date 2021/6/29 16:42
 */
public enum MoveType {

	/**
	 * 里面 前面 后面
	 */
	inner,prev,next

}
