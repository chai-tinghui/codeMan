package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;

@Mapper
@Repository
public interface CmSysButtonDao extends BaseDao<CmSysButtonEntity> {

}
