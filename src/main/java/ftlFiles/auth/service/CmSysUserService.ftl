package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import java.util.List;

/**
 * @author zrx
 */
public interface CmSysUserService {

	void add(CmSysUserEntity entity);

	void delete(CmSysUserEntity entity);

	void update(CmSysUserEntity entity);

	List<CmSysUserEntity> select(CmSysUserEntity entity);

	/**
	 * 分页获取用户
	 * @param username
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	PageData<CmSysUserEntity> pageRole(String username, Integer currentPage, Integer pageSize);

	/**
	 * 用户登录
	 * @param user
	 * @param request
	 * @return
	 */
	CmSysUserEntity doLogin(CmSysUserEntity user, HttpServletRequest request);
}
