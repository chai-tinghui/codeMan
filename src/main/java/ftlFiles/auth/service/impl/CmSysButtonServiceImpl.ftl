package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.CmSysButtonDao;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysButtonService;

@Service
public class CmSysButtonServiceImpl implements CmSysButtonService {


	private final CmSysButtonDao dao;

	@Autowired
	public CmSysButtonServiceImpl(CmSysButtonDao dao) {
		this.dao = dao;
	}

	@Override
	public void add(CmSysButtonEntity entity) {
		dao.add(entity);
	}

	@Override
	public void delete(CmSysButtonEntity entity) {
		dao.delete(entity);
	}

	@Override
	public void update(CmSysButtonEntity entity) {
		dao.update(entity);
	}

}
