package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.ExternalField;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CmSysButtonEntity extends CommonEntity implements Serializable {

	/**
     * serialVersionUID
     */
	private static final long serialVersionUID = 1L;

	/**
     *  button_id
     */
	@ApiModelProperty(value = "button_id", name = "buttonId")
	private Long buttonId;
	/**
     *  menu_id
     */
	@ApiModelProperty(value = "menu_id", name = "menuId")
	@NotNull(message = "菜单id不能为空")
	private Long menuId;
	/**
     *  button_name
     */
	@ApiModelProperty(value = "button_name", name = "buttonName")
	@NotBlank(message = "按钮名称不能为空")
	private String buttonName;
	/**
     *  module_tag_id
     */
	@ApiModelProperty(value = "module_tag_id", name = "moduleTagId")
	@NotBlank(message = "按钮编码标识不能为空")
	private String moduleTagId;

	@ApiModelProperty(value = "是否可以删除（为0则不可删除）", name = "canDel")
	private Integer canDel;

	/**
     *  create_time
     */
	@ApiModelProperty(value = "create_time", name = "createTime")
	private Date createTime;
	/**
     *  update_time
     */
	@ApiModelProperty(value = "update_time", name = "updateTime")
	private Date updateTime;
	/**
     *  create_user_id
     */
	@ApiModelProperty(value = "create_user_id", name = "createUserId")
	private Long createUserId;
	/**
     *  update_user_id
     */
	@ApiModelProperty(value = "update_user_id", name = "updateUserId")
	private Long updateUserId;

	@ApiModelProperty("角色id")
	@ExternalField
	private Long roleId;


}
