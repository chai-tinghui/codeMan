package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

import java.util.List;
<#if !entityName??>
import java.util.Map;
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import java.lang.reflect.Field;
</#if>

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.IBaseDao;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.IBaseService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.CastUtil;

<#if !entityName??>
public class BaseService implements IBaseService {

	private IBaseDao dao;

	public BaseService(IBaseDao dao) {
		this.dao = dao;
	}

	@Override
	public void add(Map<String, Object> map) {
		dao.add(map);
	}

	@Override
	public void delete(Map<String, Object> map) {
		dao.delete(map);
	}

	@Override
	public void update(Map<String, Object> map) {
		dao.update(map);
	}

	@Override
	public List<Map<String,Object>> select(Map<String, Object> map) {
		return dao.select(map);
	}

	@Override
	public Map<String, Object> likeSelect(Map<String, Object> map) {

		List<String> orderData = CastUtil.cast(map.get("orderData"));

		String orderStr = "";
		if (orderData != null) {
            for (int i = 0; i < orderData.size(); i++) {
                if (i == orderData.size() - 1) {
                    orderStr += orderData.get(i);
                    break;
                }
                orderStr += orderData.get(i) + ",";
            }
        }

		Integer currentPage = (Integer) map.get("currentPage");

		<#if dataBaseType == "mysql">
		Integer start = (currentPage - 1) * 10;
		</#if>

		<#if dataBaseType == "oracle">
		// 分页开始 页码
		Integer startIndex = (currentPage - 1) * 10 + 1;
		// 分页结束页码
		Integer endIndex = currentPage * 10;
		</#if>

		Integer totalPage = 1;

		<#if dataBaseType == "mysql">
		// sql中的start
		map.put("start", start);
		// 每页显示10条
		map.put("pageSize", 10);
		</#if>

		<#if dataBaseType == "oracle">
		// sql中的startIndex
		map.put("startIndex", startIndex);
		// sql中的endIndex
		map.put("endIndex", endIndex);
		</#if>

		//排序条件
		map.put("orderStr", orderStr);


		// 获取总个数
		Integer totalCount = dao.likeSelectCount(map).intValue();

		List<Map<String, Object>> resultList = dao.likeSelect(map);

		if (totalCount != 0) {

			if (totalCount % 10 == 0) {
				totalPage = totalCount / 10;
			} else {
				totalPage = totalCount / 10 + 1;
			}

		}

		map.clear();
		// 当前页
		map.put("currentPage", currentPage);

		map.put("count", totalCount);

		map.put("totalPage", totalPage);

		map.put("result", resultList);

		return map;
	}

	@Override
	public void batchAdd(List<Map<String, Object>> list) {
		dao.batchAdd(list);
	}

	@Override
	public void batchDelete(List<Map<String, Object>> list) {
		dao.batchDelete(list);
	}

	@Override
	public void batchUpdate(List<Map<String, Object>> list) {
		dao.batchUpdate(list);
	}

}
<#else>
public class BaseService<E> implements IBaseService<E> {

	public IBaseDao dao;

	public BaseService(IBaseDao dao) {
		this.dao = dao;
	}

	@Override
	public void add(E entity) {
		dao.add(entity);
	}

	@Override
	public void delete(E entity) {
		dao.delete(entity);
	}

	@Override
	public void update(E entity) {
		dao.update(entity);
	}

	@Override
	public List<E> select(E entity) {
		return dao.select(entity);
	}

	@Override
	public PageData<E> likeSelect(E entity) {

		List<String> orderData = null;
        Integer currentPage = null;
        Class<?> entityClass = entity.getClass();

        try {
            Field orderDataFiled = entityClass.getSuperclass().getDeclaredField("orderData");
            orderDataFiled.setAccessible(true);
            orderData = CastUtil.cast(orderDataFiled.get(entity));

            Field currentPageFiled = entityClass.getSuperclass().getDeclaredField("currentPage");
            currentPageFiled.setAccessible(true);
            currentPage = CastUtil.cast(currentPageFiled.get(entity));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String orderStr = "";
        if (orderData != null) {
            for (int i = 0; i < orderData.size(); i++) {
                if (i == orderData.size() - 1) {
                    orderStr += orderData.get(i);
                    break;
                }
                orderStr += orderData.get(i) + ",";
            }
        }


        <#if dataBaseType == "mysql">
		Integer start = (currentPage - 1) * 10;
		</#if>

		<#if dataBaseType == "oracle">
		// 分页开始 页码
		Integer startIndex = (currentPage - 1) * 10 + 1;
		// 分页结束页码
		Integer endIndex = currentPage * 10;
		</#if>

        Integer totalPage = 1;

        try {

        <#if dataBaseType == "mysql">
        	Field startFiled = entityClass.getSuperclass().getDeclaredField("start");
            startFiled.setAccessible(true);
            // sql中的start
            startFiled.set(entity,start);

            Field pageSizeFiled = entityClass.getSuperclass().getDeclaredField("pageSize");
            pageSizeFiled.setAccessible(true);
            // 每页显示10条
            pageSizeFiled.set(entity,10);
        </#if>
        <#if dataBaseType == "oracle">
        	Field startIndexFiled = entityClass.getSuperclass().getDeclaredField("startIndex");
            startIndexFiled.setAccessible(true);
            // sql中的start
            startIndexFiled.set(entity,startIndex);

            Field endIndexFiled = entityClass.getSuperclass().getDeclaredField("endIndex");
            endIndexFiled.setAccessible(true);
            // 每页显示10条
            endIndexFiled.set(entity,endIndex);
        </#if>


            Field orderStrFiled = entityClass.getSuperclass().getDeclaredField("orderStr");
            orderStrFiled.setAccessible(true);
            //排序条件
            orderStrFiled.set(entity,orderStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 获取总个数
        Integer totalCount = dao.likeSelectCount(entity).intValue();

        List<E> resultList = dao.likeSelect(entity);

        if (totalCount != 0) {

            if (totalCount % 10 == 0) {
                totalPage = totalCount / 10;
            } else {
                totalPage = totalCount / 10 + 1;
            }

        }

        PageData<E> pageData = new PageData<>();

        // 当前页
        pageData.setCurrentPage(currentPage);

        pageData.setCount(totalCount);

        pageData.setTotalPage(totalPage);

        pageData.setResult(resultList);

        return pageData;
	}

	@Override
	public void batchAdd(List<E> list) {
		dao.batchAdd(list);
	}

	@Override
	public void batchDelete(List<E> list) {
		dao.batchDelete(list);
	}

	@Override
	public void batchUpdate(List<E> list) {
		dao.batchUpdate(list);
	}

}
</#if>
