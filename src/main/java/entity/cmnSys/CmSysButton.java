package entity.cmnSys;

import java.util.Date;

/**
 * @ClassName CmSysButton
 * @Author zrx
 * @Date 2021/5/18 10:58
 */
public class CmSysButton {
	private Long buttonId;
	private Long menuId;
	private String buttonName;
	/**
	 * 组件标签id
	 */
	private String moduleTagId;
	private Date createTime;
	private Date updateTime;
	private Long createUserId;
	private Long updateUserId;

	public Long getButtonId() {
		return buttonId;
	}

	public void setButtonId(Long buttonId) {
		this.buttonId = buttonId;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getModuleTagId() {
		return moduleTagId;
	}

	public void setModuleTagId(String moduleTagId) {
		this.moduleTagId = moduleTagId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
}
