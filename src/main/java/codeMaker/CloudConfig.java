package codeMaker;

import constant.CodeConstant;
import entity.Parameters;
import org.apache.commons.lang3.StringUtils;
import util.CodeWriterUtil;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CloudConfig extends JFrame {


	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7654101057239885726L;
	private static volatile CloudConfig frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				CloudConfig frame = new CloudConfig(new Parameters());
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Launch the application.
	 */
	public synchronized static void init(Parameters parameters) {
		EventQueue.invokeLater(() -> {
			try {
				//如果不为空，关闭之前的 frame
				if (frame != null) {
					frame.dispose();
					frame = null;
				}
				frame = new CloudConfig(parameters);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private CloudConfig(Parameters parameters) {
		setResizable(true);
		setTitle("SpringCloud配置");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MakeCode.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 441, 309);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel registerCenterLabel = new JLabel("注册中心");
		JComboBox<String> registerCenterComboBox = new JComboBox<>();
		registerCenterComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"eureka", "nacos"}));
		//设置注册中心
		registerCenterComboBox.setSelectedItem(parameters.getCloudRegiseterCenter());
		JLabel networkLabel = new JLabel("网关");
		JComboBox<String> networkCombox = new JComboBox<>();
		networkCombox.setModel(new DefaultComboBoxModel<>(new String[]{"zuul", "gateway"}));
		//设置网关
		networkCombox.setSelectedItem(parameters.getCloudNeteWork());

		JLabel lblNewLabel = new JLabel("基础配置");
		lblNewLabel.setFont(new Font("微软雅黑", Font.BOLD, 16));

		JLabel cloudSysCnNameLabel = new JLabel("系统中文名称");
		JTextField cloudSysCnNameField = new JTextField();
		cloudSysCnNameField.setColumns(10);
		cloudSysCnNameField.setText(parameters.getCloudSysName());

		JLabel cloudSysNameLabel = new JLabel("系统英文名称");
		JTextField cloudSysNameField = new JTextField();
		cloudSysNameField.setColumns(10);
		cloudSysNameField.setText(parameters.getCloudSysEngName());

		JButton baseSaveBtn = new JButton("保存");
		JCheckBox onlyMakeSerCheckBox = new JCheckBox("仅生成微服务模块");
		JButton configDecrBtn = new JButton("配置说明");

		//监听保存按钮事件
		baseSaveBtn.addActionListener(e -> {
			//设置属性
			String center = (String) registerCenterComboBox.getSelectedItem();
			String sysName = cloudSysNameField.getText();
			parameters.setCloudSysName(StringUtils.isBlank(cloudSysCnNameField.getText()) ? "cloud" : cloudSysCnNameField.getText());
			parameters.setCloudSysEngName(StringUtils.isBlank(sysName) ? "cloud" : CodeWriterUtil.spStrFilter(sysName));
			parameters.setCloudRegiseterCenter(center);
			parameters.setCloudNeteWork((String) networkCombox.getSelectedItem());
			parameters.setOnlyCloudModel(onlyMakeSerCheckBox.isSelected());
			if (CodeConstant.NACOS.equals(center)) {
				JOptionPane.showMessageDialog(null, "提示：选择nacos注册中心需要在本地启动nacos服务，否则生成后的程序将无法正常运行！" + CodeConstant.NEW_LINE + CodeConstant.NEW_LINE
								+"nacos下载配置可参考公众号——螺旋编程极客的《SpringCloud Alibaba：理论+实践通关微服务灵魂摆渡者—Nacos》"
						, "提示",
						JOptionPane.INFORMATION_MESSAGE);
			}
			JOptionPane.showMessageDialog(null, "保存成功", "提示",
					JOptionPane.INFORMATION_MESSAGE);
		});

		//配置说明监听
		configDecrBtn.addActionListener(e -> {
			JOptionPane.showMessageDialog(null,
					"系统中文名称为代码生成后前台展示的名称" + CodeConstant.NEW_LINE + CodeConstant.NEW_LINE +
							"网关和注册中心根据自身需要选择" + CodeConstant.NEW_LINE + CodeConstant.NEW_LINE +
							"如果之前已经生成过公共模块，建议勾选《仅生成微服务模块》，避免重复生成覆盖已有项目",
					"说明",
					JOptionPane.INFORMATION_MESSAGE);
		});


		JPanel basePanel = new JPanel();
		//样式布局
		GroupLayout glContentPane = new GroupLayout(contentPane);
		glContentPane.setHorizontalGroup(
				glContentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(glContentPane.createSequentialGroup()
								.addGap(23)
								.addComponent(basePanel, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
								.addContainerGap())
		);
		glContentPane.setVerticalGroup(
				glContentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(glContentPane.createSequentialGroup()
								.addContainerGap()
								.addComponent(basePanel, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(282, Short.MAX_VALUE))
		);

		GroupLayout glBasePanel = new GroupLayout(basePanel);
		glBasePanel.setHorizontalGroup(
				glBasePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(glBasePanel.createSequentialGroup()
								.addGroup(glBasePanel.createParallelGroup(Alignment.LEADING)
										.addGroup(glBasePanel.createSequentialGroup()
												.addContainerGap()
												.addComponent(lblNewLabel))
										.addGroup(glBasePanel.createSequentialGroup()
												.addGap(119)
												.addComponent(baseSaveBtn)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addComponent(configDecrBtn))
										.addGroup(glBasePanel.createSequentialGroup()
												.addContainerGap()
												.addGroup(glBasePanel.createParallelGroup(Alignment.LEADING)
														.addGroup(glBasePanel.createSequentialGroup()
																.addComponent(cloudSysCnNameLabel)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(cloudSysCnNameField, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.UNRELATED)
																.addComponent(cloudSysNameLabel)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(cloudSysNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
														.addGroup(glBasePanel.createSequentialGroup()
																.addComponent(registerCenterLabel)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(registerCenterComboBox, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(onlyMakeSerCheckBox))
														.addGroup(glBasePanel.createSequentialGroup()
																.addComponent(networkLabel)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(networkCombox, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)))))
								.addContainerGap(80, Short.MAX_VALUE))
		);
		glBasePanel.setVerticalGroup(
				glBasePanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(glBasePanel.createSequentialGroup()
								.addContainerGap()
								.addComponent(lblNewLabel)
								.addGap(18)
								.addGroup(glBasePanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(cloudSysCnNameLabel)
										.addComponent(cloudSysCnNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(cloudSysNameLabel)
										.addComponent(cloudSysNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(glBasePanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(registerCenterLabel)
										.addComponent(registerCenterComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(onlyMakeSerCheckBox))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(glBasePanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(networkCombox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(networkLabel))
								.addGap(18)
								.addGroup(glBasePanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(configDecrBtn)
										.addComponent(baseSaveBtn))
								.addGap(71))
		);
		basePanel.setLayout(glBasePanel);
		contentPane.setLayout(glContentPane);
	}
}
